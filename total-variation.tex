\documentclass{article}

\usepackage{amsmath}
%\usepackage{amsfonts}
\usepackage{amsthm}
%\usepackage{amssymb}
%\usepackage{mathrsfs}
%\usepackage{fullpage}
%\usepackage{mathptmx}
%\usepackage[varg]{txfonts}
\usepackage{natbib}
\usepackage{color}
\usepackage[charter]{mathdesign}
\usepackage[pdftex]{graphicx}
%\usepackage{float}
%\usepackage{hyperref}
%\usepackage[modulo, displaymath, mathlines]{lineno}
%\usepackage{setspace}
%\usepackage[titletoc,toc,title]{appendix}

%\linenumbers
%\doublespacing

\theoremstyle{definition}
\newtheorem*{defn}{Definition}
\newtheorem*{exm}{Example}

\theoremstyle{plain}
\newtheorem*{thm}{Theorem}
\newtheorem*{lem}{Lemma}
\newtheorem*{prop}{Proposition}
\newtheorem*{cor}{Corollary}

\newcommand{\argmin}{\text{argmin}}
\newcommand{\ud}{\hspace{2pt}\mathrm{d}}
\newcommand{\bs}{\boldsymbol}
\newcommand{\PP}{\mathsf{P}}

\title{Total variation regularization for estimating the fluidity of ice shelves}
\author{Daniel Shapero}
\date{}

\begin{document}

\maketitle

\begin{abstract}
To predict future sea-level rise, glaciologists use numerical models of ice flow that are initialized from estimates of the current state of the ice sheets.
The surface velocity, surface elevation, and thickness of a glacier can be measured via satellite and airborne remote sensing.
The fluidity and basal friction of a glacier, however, are not readily observable at large scales.
To initialize ice sheet forecasts, glaciologists use inverse methods to estimate the unobservable fields from the observable ones.
These inverse problems are ill-posed and require some form of regularization, without which the estimated field can be polluted with spurious oscillations.
Nearly all of the glaciological literature on this problem has used a regularization functional that favors smooth solutions.
In this work, we will argue that using the \emph{total variation} of the inferred field as a regularization functional better encapsulates our prior knowledge from ice physics.
Finally, we demonstrate the method for inferring the fluidity of the Larsen C Ice Shelf in the Antarctic Peninsula.
\end{abstract}


\section{Introduction}

Predicting the future volume of the Greenland and Antarctic ice sheets is one of the central research problems facing glaciologists today.
To create these forecasts, an estimate of the current state of the ice sheets can be projected forward in time using numerical models of glacier flow.
This initial state consists of several fields: velocity, surface elevation, fluidity, bed elevation, and bed friction.
Depending on the sophistication of the numerical model, the fluidity or bed friction might in turn be described in terms of other fields.
For example, the ice fluidity can be parameterized in terms of the temperature, impurity content, meltwater fraction, and large-scale fracture density.
Likewise, the bed friction might be expressed as a function of the effective pressure within the subglacial hydrological system and a geological roughness factor.

Some of the fields necessary to initialize ice sheet forecasts can be observed at large scales while others cannot.
The surface elevation and surface velocity can be measured via satellite remote sensing, and recent missions such as Landsat-8 and ICESat-2 have dramatically increased the volume of data available to the glaciological community.
Ice thickness, on the other hand, can only be measured at large scales using airborne radio echo sounding.
Airborne surveying necessarily has much coarser spatio-temporal coverage than satellite observation.
To improve estimates of ice thickness derived using kriging, measurements of surface elevation change rate and estimates of surface mass balance from regional climate models can also be included as constraints.
This idea was used to create the BedMachine datasets for Greenland and Antarctica \citep{Morlighem2011-la}.

Despite these advances in remote sensing, fluidity and basal friction are not directly observable at large scales.
To estimate the initial state of the ice sheets for use in forecasts, glaciologists have turned to \emph{inverse methods} to estimate the unobservable fields from the observable ones.
Most studies in the literature use measurements $u^o$ of the ice velocity to estimate some proxy $\theta$ for the fluidity or basal friction.
The inverse problem amounts to finding some ``best'' value of $\theta$ subject to the model physics and the observations, which can then be posed as a constrained optimization problem.

\subsection{Prior work}

\citet{MacAyeal1992-wf} first applied this \emph{control method} for estimating the basal friction underneath Ice Stream E in Antarctica.
The inferred basal friction showed isolated regions of high basal resistance, which was taken as additional confirmation of the hypothesis that ``sticky spots'' are present amidst the otherwise weak beds of the Siple Coast ice streams.
Subsequent work applied the control method to new areas and new physics models.
The availability of more and better observational data from satellite radar made it possible to estimate the basal friction of all the Siple Coast ice streams \citep{Joughin2004-vz} and later of the entire Greenland and Antarctic continents \citep{Gillet-Chaulet2012-uf, Morlighem2013-ix}.
The same methodology has also been applied to estimate the viscosity of ice shelves \citep{Rommelaere1997-ig, Vieli2006-ii, Khazendar2007-ft} and using higher-order models of glacier flow \citep{Morlighem2010-em, Goldberg2011-ej, Brinkerhoff2013-wy}.
The state estimates obtained from these kinds of studies have been used both for making forecasts of future ice extent \citep{Joughin2014-rj} and for exploring hypotheses about poorly-understood aspects of glacier physics \citep{Sergienko2013-ao}.

The control method can also be given a statistical interpretation \citep{Raymond2009-lc}.
Minimizing the objective functional in the control method is equivalent to finding the \emph{maximum a posteriori probability} (MAP) estimator in Bayesian inference.
The model-data misfit functional is identified with the negative logarithm of the likelihood, while the regularization functional is identified with the negative logarithm of the prior density.
We will not repeat the derivation of how these two approaches are equivalent here but instead refer to the discussion in section 3 of \citet{Arthern2015-su}.
While the MAP estimate can give a good picture of what the true parameters are, the statistical interpretation can also be used for uncertainty quantification \citep{Isaac2015-ci}.
The control method, on the other hand, is focused solely on obtaining a single estimate of the unknown parameters.

The Bayesian perspective also offers new insight into how to choose the regularization functional by applying the \emph{maximum entropy principle} \citep{Arthern2015-su}.
This principle states that the prior distribution (equivalently the regularization functional) should be chosen as the least informative distribution that is consistent with all pre-existing knowledge about the parameters.
It then falls to modelers to determine what pre-existing knowledge they do or do not have.
Prior knowledge can come from basic physical principles, such as conservation laws or the laws of thermodynamics; it can also come from previous direct measurements.
The most commonly applied regularization functional in the glaciology literature, the squared $H^1$-norm of the inferred field, is usually justified on the grounds that the results are free of unrealistic oscillations.
We propose that a different regularization functional, the \emph{total variation}, better encapsulates what we know about the physics of heat and damage transport in ice shelves.


\subsection{Inverse problems}

In the following, we will focus on inferring the fluidity of floating ice shelves.
For grounded ice streams, both the basal friction and fluidity are unknown, and there is no unique best estimate for both fields at once \citep{Arthern2010-qp}.
Using a modeled temperature field to estimate the fluidity can resolve some of the ambiguity, but the modeled temperature depends on the basal friction field that is being inferred in the first place \citep{Seroussi2013-ac}.
More sophisticated closure schemes have been proposed that do substantially improve matters but none of them is entirely satisfactory \citep{Gladstone2014-we, Arthern2015-bc}.
Ice shelves, by contrast, experience little to no basal friction and thus serve as a better test case for inverse problem solvers.
The considerations we describe here nontheless apply equally well to more complex problems such as inferring basal friction.

There are three ingredients in an inverse problem.
First is the physics model, which relates the unobservable parameters with some observable field.
In our case, the observable field will be the ice velocity $u$ and the parameter to be estimated is the spatially-varying fluidity coefficient $A$ in Glen's flow law
\begin{equation}
    \dot\varepsilon = A\tau^n,
\end{equation}
where $n = 3$ for polycrystalline ice.
The fluidity coefficient must be positive, so it is common practice to re-parameterize $A$ in terms of some auxiliary field $\theta$ in order to enforce this constraint.
In our case, we will write
\begin{equation}
    A = A_0e^{\theta}
\end{equation}
and instead infer $\theta$.
Since we are studying floating ice shelves, we will use the shallow shelf approximation as our physics model.
In general, the physics model can be described as an abstract nonlinear equation
\begin{equation}
    F(u, \theta) = 0.
    \label{physics-model}
\end{equation}
See the appendix for the shallow shelf equations.

The next ingredient is a set of observations $u^o$ together with some measure of how much a solution $u$ of the physics model of equation \eqref{physics-model} differs from observations.
The most common choice in the literature is the mean-square error
\begin{equation}
    E(u - u^o) = \frac{1}{2}\int_\Omega(u - u^o)^*\left[\begin{matrix}\sigma_x^{-2} & 0 \\ 0 & \sigma_y^{-2}\end{matrix}\right](u - u^o)\ud x,
    \label{eq:model-data-misfit}
\end{equation}
where $\sigma_x$, $\sigma_y$ are the standard deviations of the measurement errors in each direction.
Using the mean-square error is equivalent to the assumption that the measurement errors are normally distributed, but this may not be the case.
For example, \citet{Morlighem2010-em} propose adding another term to the error metric with the aim of achieving a closer fit to observations in slower-flowing areas of the ice sheet.
While this work did not adopt the statistical viewpoint, their choice of error metric is equivalent to using a heavier-tailed distribution than the normal.

The last ingredient is the regularization functional $R$, which penalizes spurious oscillations or other unphysical features in the inferred field $\theta$.
The most common choice of regularization functional $R$ in the literature is the square norm of the gradient of $\theta$:
\begin{equation}
    R_\alpha(\nabla\theta) = \frac{\alpha^2}{2}\int_\Omega|\nabla\theta|^2\ud x
\end{equation}
where $\alpha$ is a length scale which we will refer to as the \emph{filter length}.
In the inverse problems literature, this is referred to as the squared $H^1$-norm because of the connection to the Sobolev space $H^1(\Omega)$ of fields defined on the domain $\Omega$ with square-integrable gradients.
The theory behind this is unimportant for the following but we will adopt the terminology.

These three ingredients are combined into one \emph{objective functional}
\begin{equation}
    J(\theta, u, \lambda) = E(u - u^o) + R_\alpha(\nabla\theta) + F(u, \theta)\cdot\lambda,
\end{equation}
where we have introduced the \emph{adjoint state} variable $\lambda$.
The adjoint state is a Lagrange multiplier that enforces the model physics $F(u, \theta) = 0$.

In addition to the form of the regularization functional, we also need to decide how much to weight the regularization as opposed to the model-data misfit.
This amounts to deciding on the right value of the filering length $\alpha$.
Two common approaches in the glaciological literature are the L-curve method \citep{hansen1992analysis, shapero2016basal} and the discrepancy principle \citep{aster2018parameter, habermann2012reconstruction}.
Both of these approaches find a value of the filtering length that gives the best tradeoff between goodness of fit and parsimony or simplicity of the estimated parameters.
The L-curve method is arguably more of a heuristic approach, while the discrepancy principle more directly takes into account the statistical distribution of the measurement errors.
Selecting the filtering length is a separate issue from our main concern of what regularization functional to use in the first place, so we do not undertake this analysis in the following.
Instead, we will pick a value of the filtering length that gives reasonable results and compare the outcomes for the two different functionals.


\section{Methods}

\begin{table}[h]
    \begin{tabular}{l|l}
        Symbol & Meaning \\
        \hline
        $h$ & thickness \\
        $b$ & bed elevation  \\
        $s$ & surface elevation  \\
        $u$ & velocity \\
        $\dot\varepsilon$ & strain rate, $\frac{1}{2}(\nabla u + \nabla u^\top)$\\
        $\tau$ & stress tensor \\
        $C$ & bed friction coefficient \\
        $A$ & fluidity coefficient \\
        $Q$ & activation energy \\
        $R$ & universal gas constant \\
        $\rho$ & ice density \\
        $c$ & specific heat capacity \\
        $k$ & thermal conductivity
    \end{tabular}
    \caption{Mathematical symbols}
    \label{tab:mathematical-symbols}
\end{table}


We will explore an alternative choice to the commonly-applied square $H^1$-norm for inferring the fluidity of a floating ice shelf and then compare the resulting estimates.
Our alternative regularization functional, the \emph{total variation} norm, is motivated by the fact that it better preserves features that we expect of the fluidity.
Ice fluidity is a function of four factors: temperature, damage, crystal fabric, and impurity content.
We will discuss models for temperature and damage in some detail.
These two quantities are transported largely through advection rather than diffusion, which will motivate our use of total variation regularization.
We will not focus at all on anisotropy or impurities.

\subsection{Motivation for TV}

Ice fluidity is related to ice temperature through an Arrhenius law:
\begin{equation}
    A(T) = A_0e^{-Q/RT},
\end{equation}
where $A_0$ is a prefactor with units of stress${}^{-n}$ $\cdot$ time${}^{-1}$ \citep{Cuffey2010-im}.
Heat transport in glaciers is described by an advection-diffusion equation:
\begin{equation}
    \left(\frac{\partial}{\partial t} + \nabla\cdot u\right)\rho cT = \nabla\cdot k\nabla T + \dot\varepsilon : \tau.
\end{equation}
The thermal diffusivity of ice is $\alpha = k / \rho c \approx 40$ m${}^2$/year.
Consequently, the horizontal P\'eclet number for heat flow in ice is on the order of 10${}^7$ assuming length scales of 100 km and velocities of 100 m/yr, implying that advection is the dominant mode of horizontal transport rather than diffusion.

In addition to temperature, crevasses and rifts are often modeled as directly affecting the fluidity in large-scale simulations for which tracking individual crevasses would be prohibitively expensive.
These continuum damage mechanics models define a scalar \emph{damage field} $D$ such that the large-scale, effective fluidity $A'$ is
\begin{equation}
    A' = A(T, \ldots) / (1 - D)^n
\end{equation}
where $A(T, \ldots)$ is the fluidity in the absence of crevasses as a function of temperature and other factors.
Intact, undamaged ice has $D = 0$, while heavily crevassed ice has $D$ approaching 1.
These models then prescribe an evolution equation for how the damage field changes in time.
\citet{Albrecht2012-qh} propose the model
\begin{equation}
    \left(\frac{\partial}{\partial t} + u\cdot\nabla\right)D = f_o\cdot(1 - D) - f_c\cdot D
    \label{albrecht-levermann}
\end{equation}
where $f_o$ represents crevasse opening and $f_c$ represents crevasse closing.
The source terms are only activated when some variable (e.g. stress or strain rate) exceeds some threshold value.
Several variations on this model exist depending on which field the source terms or the threshold depend on.
The particular form is unimportant for the discussion that follows, only that the damage field is thought to be transported entirely by advection.

The most important characteristic of advective transport is that discontinuities in the initial profile remain sharp.
Under diffusive transport these features would smooth out over time.
In some areas, we expect discontinuities or very sharp gradients to occur.
For example, consider two neighboring grounded tributaries of a floating ice shelf that have different surface slopes, and which then merge in a suture zone over the floating ice shelf.
The strain heating that each tributary undergoes will be different, so there will be a sharp temperature gradient across the suture zone.
This sharp gradient will persist downstream because the residence time of the ice shelf is much shorter than the time necessary for the temperature gradient to be diffused out.

If sharp gradients or discontinuities are possible in any of the fields that make up the ice fluidity, we should choose a regularization functional that also permits these features.
The $H^1$-norm of a discontinuous field, however, is infinite.
Using this regularization functional goes against what we know about physical processes in real glaciers.
From the Bayesian perspective, this amounts to using a prior distribution that does not adequately reflect our prior knowledge.

\begin{figure}[t]
    \begin{center}\includegraphics[width=0.5\linewidth]{demos/h1-tv/h1-tv.png}\end{center}
    \caption{The smooth bump in (A) has small $H^1$-norm and small TV norm.
    The field in (B) has large $H^1$ and TV norms because of the oscillations.
    The field in (C) has an infinite $H^1$ norm because of the jump discontinuity, but it has a small TV norm because the discontinuity is confined to a relatively small perimeter.
    The field in (D) also has an infinite $H^1$ norm as well, but a large TV norm because the discontinuities occur over a longer interface.}
    \label{fig:h1-tv}
\end{figure}

Instead, we should seek a regularization functional that penalizes unphysical oscillations while still permitting sharp gradients or discontinuities in the inferred field.
The \emph{total variation} (TV) functional
\begin{equation}
    R_\alpha(\nabla\theta) = \alpha\int_\Omega|\nabla\theta|\ud x
\end{equation}
fulfills these requirements \citep{aster2018parameter}.
Despite the fact that a discontinuous field does not have a pointwise gradient in the usual sense, the integral above is well-defined.
The total variation of a discontinuous field is proportional to the height of the discontinuities and the perimeter along which they occur.
TV is still an effective regularizer because smooth but highly oscillatory fields still have large total variation.
An illustration of different fields with small and large values of the $H^1$ and total variation norms is shown in figure \ref{fig:h1-tv}.

\subsection{Application to Larsen C Ice Shelf}

To explore the effects of using different regularization functionals, we estimated the fluidity of the Larsen C Ice Shelf in the Antarctic Peninsula using both the squared $H^1$-norm and the total variation norm as regularization functionals.
We used the glacier flow modeling library \emph{icepack} for all computations \cite{shapero2021icepack}.
Nonetheless, our results are not specific to this model and TV regularization can be implemented in other models such as ISSM or Elmer/Ice.

For the input data, we used version 2.0 of the BedMachine ice thickness map \citep{morlighem2020deep} and the MEaSUREs interferometric SAR phase-based velocity map \citep{mouginot2019continent} of Antarctica.
We used the error estimates from the MEaSUREs velocity dataset as the standard deviations $\sigma_x$, $\sigma_y$ in the model-data misfit functional in equation \eqref{eq:model-data-misfit}.
These are only formal error estimates, however, and are thus overly optimistic in terms of how good a model should fit the data \textcolor{red}{run this by Ian or Ben}.
We used a filtering length of 7.5 km for both the squared $H^1$-norm case and the TV functional.
This value was found to fit the data well but not overfit and to preserve detail in the inferred log-fluidity without obviously spurious oscillations.
A full analysis using the discrepancy principle would require deciding what is a reasonable standard deviation to fit to, which is likely to be larger than the formal error estimates.
Since our purpose is only to explore the differences between regularization functionals and not on tuning the filter length as such, we did not carry out this analysis.
The observational data that we used predate the calving of Iceberg A-68 in July 2017 \citep{hogg2017impacts}.

We generated an unstructured triangular mesh for this domain from a hand-digitized outline of the ice shelf using the software gmsh \citep{geuzaine2009gmsh}.
The mesh has a nominal resolution of 10 km, but we used piecewise quadratic basis functions, giving a resolution of 5 km.
We found quadratic basis functions to give better accuracy for less work than linear basis functions on a more refined mesh.
The results at a given resolution should nonetheless be independent of the mesh and basis as long the solution has not been grossly overfit to the noise.

Using total variation regularization can be more complex because the TV functional is non-smooth.
We worked around this difficulty by using the alternating direction method of multipliers; see Appendix \ref{sec:admm} for details.


\section{Results}


\begin{figure}[t]
\includegraphics[width=\linewidth]{demos/larsen/parameters-7500.png}
\caption{Logarithm of the fluidity of Larsen C Ice Shelf inferred using the squared $H^1$ norm (left) and total variation (right).}
\label{fig:larsen-parameters}
\end{figure}

The inferred log-fluidity with each regularization functional is shown in figure \ref{fig:larsen-parameters}.
The two estimates show a similar broad overall pattern with roughly the same mean value of the fluidity.
The TV-regularized estimate shows more extreme negative values corresponding to stiffer ice than the $H^1$-regularized estimate.
Both estimates have about the same upper bound, corresponding to more deformable ice.
The two estimates also produce similar features to each other in terms of location and spatial pattern; where one estimate shows stripes of weaker ice due to crevasses, so does the other.

The main differences between the two estimates are in the sharpness of individual features.
We highlight a few regions where the differences are largest.
Figure \ref{fig:larsen-details} shows the fluidity fields with both $H^1$ and TV regularization downstream of Cabinet, Mill, and Whirlwind inlets.
The stiffer suture zones extending along flow intersect with crevasses oriented normal to the flow direction at the juncture between Mill and Whirlwind inlets.
With $H^1$ regularization, the crevasses present as smoothly-varying sinusoids, but with TV regularization the contrasts are sharper.
The boundary between the stiff suture zone and the neighboring ice is also much sharper.

\begin{figure}[t]
    \includegraphics[width=0.64\linewidth]{demos/larsen/larsen.png}
    \includegraphics[width=0.35\linewidth]{demos/larsen/inflow-7500.png}
    \caption{Left: Velocity streamlines of Larsen C.
    Red box outlines the area of figures on the right.
    Labels are Cabinet (C), Mill (M), Whirlwind (W), Seligmann (S), and Trail (T) inlets, Holick-Kenyon (HK) peninsula, Gipps Ice Rise (G).
    Right: log-fluidity near Cabinet, Mill, and Whirlwind inlets with $H^1$ regularization (top) and TV regularization (bottom).
    The rifts coincide with the merger of ice flowing in from Mill and Whirlwind inlets.
    The stronger ice downstream of Cabinet Inlet, on the other hand, does not follow the velocity field at all.}
    \label{fig:larsen-details}
\end{figure}

Our estimates show many of the same features as \citet{khazendar2011acceleration}, including weakening near the Gipps and Hearst rifts and the rifts downstream from Hollick-Kenyon Peninsula, as well as stronger ice downstream of Churchill Peninsula.
The alternating pattern of weaker ice that we find between Mill and Whirlwind inlets, as shown in figure \ref{fig:larsen-details}, was not seen in previous work.
Since these features are present with both $H^1$ and TV regularization, albeit with sharper definition in the latter, we attribute this difference to our use of higher-resolution observational data than was available at the time of this previous study.
The suture zone between Whirlwind and Seligmann inlets further to grid east shows no such pattern, while the zone between Seligmann and Trail inlets does but to a much lesser degree.

\section{Discussion}

Regularizing glaciological inverse problems using the total variation norm gives results that agree in the large scale with the squared $H^1$-norm, but which differ in the sharpness of smaller-scale details.
We argue that TV regularization is the better choice because we expect such sharp small-scale features on physical grounds.
The $H^1$ norm, on the other hand, diffuses these features out.

Whether this level of detail is necessary depends on the study objectives.
Several published works that used inverse methods to estimate the fluidity of ice shelves interpret relatively narrow features of the fluidity as evidence of physical processes that are not easily observable.
For example, \citet{Vieli2006-ii} used changes in the estimated fluidity of Larsen B Ice Shelf from 1995 to 1999 to show that material weakening, as opposed to overall thinning or tributary speedup, is necessary to explain the speedup of the ice shelf over this time period.
They found a large relative weakening and sharp gradients in fluidity over narrow (< 5km) regions near the shelf margins.
If the interpretation of the inferred fluidity hinges on knowledge of where sharp gradients occur, then TV regularization is better suited to this analysis.

The ability to accurately infer sharp gradients in the fluidity is especially important in light of recent work on continuum damage mechanics (CDM) models.
\citet{borstad2013creep} estimated the damage state of Larsen C by calculating the ice fluidity from a modeled temperature, inferring the fluidity of the ice shelf from observations using a data assimilation algorithm, and taking damage to account for any discrepancy between the two.
If a physically unfounded regularization functional is used to estimate the fluidity and thus the damage field, forward model projections using CDM might appear to do a poor job reproducing subsequent measurements when in fact the model was merely initialized wrong.
Data assimilation can be a useful tool for evaluating as-yet unproved models of physical processes in glaciology, but if the modeler unwittingly injects wrong assumptions into the procedure via the choice of regularization functional then a good model might appear worse than it really is.

\begin{figure}[t]
    \includegraphics[width=0.43\linewidth]{demos/larsen/transect-location.png}
    \includegraphics[width=0.55\linewidth]{demos/larsen/transect-thickness-fluidity.png}
    \caption{Left: Larsen C Ice Shelf, transect across the Mill-Whirlwind suture zone in blue. Right: transect of the inferred log-fluidity (blue), raw thickness values (dashed orange), and the smoothed thickness used in the assimilation procedure (solid orange).}
    \label{fig:larsen-transect}
\end{figure}

Previous work found weaker ice that coincided with areas of clear full-depth rifting in optical satellite imagery, for example downstream of Hollick-Kenyon Peninsula \citep{khazendar2011acceleration}.
We found alternating bands of weaker and stronger ice at the suture zone downstream of Mill and Whirlwind inlets (see figure \ref{fig:larsen-details}).
These features had not been detected previously; we were able to do so by assimilating newer high-resolution data.
There are several possible explanations for this feature.
First, they might be an artifact of a noisy thickness or velocity field.
Figure \ref{fig:larsen-transect} shows a transect of the observed velocity and thickness and the inferred field.
The raw value of the thickness field from the BedMachine dataset does have an oscillatory pattern in this region of a similar wavelength.
But the thickness field we used in the assimilation had a smoothing kernel applied in order to eliminate unphysical large values of the driving stress.
The smoothed thickness field, also shown in figure \ref{fig:larsen-transect}, does not possess oscillations at the frequency found in the inferred fluidity, so we consider it unlikely that this is merely the response to a data artifact.
Second, the banded features might be a consequence of crevasse-induced weakening.
This region of the ice shelf lies at the suture zone between two flow units and thus experiences a relatively high strain rate, but the surface is not as obviously crevassed in Landsat imagery as, say, the full-depth rifts downstream of Hollick-Kenyon Peninsula.
Several IceBridge radar flightlines pass over the Mill-Whirlwind suture zone, and the basal returns are not indicative of either marine ice or basal crevasses.
\textcolor{red}{Show a radargram or two.}
Consequently, we find it unlikely that either surface or basal crevasses caused the banded features.
Having ruled out these two explanations, we hypothesize that the pattern we found between Mill and Whirlwind inlets is the hallmark of large-scale anisotropy at the suture zone between two flow units.
We assumed in our model formulation that the fluidity is a scalar field.
To capture large-scale fabric effects that make the membrane stress respond differently depending on the the principal strain rate directions, we should instead treat the fluidity as a higher-rank tensor field.
The features that we inferred are the best explanation of a fundamentally anisotropic phenomenon using only an isotropic field.
Repeating the experiment using a tensor fluidity would eliminate the oscillations and this experiment will be the subject of future work.
The considerations we have raised here about which regularization functional to use become even more important for the tensor case, which has more degrees of freedom and is thus even more under-constrained.
Nonetheless, whether the feature is real or not is orthogonal to the main point of the present investigation.

TV regularization is a good choice for inverse problems when the inferred field may have sharp gradients, but it is not the only reasonable choice.
For example, one could instead use the \emph{anisotropic} $H^1$ norm, where the ice velocity defines a preferred direction of smoothing:
\begin{equation}
    R_\alpha(\nabla\theta) = \frac{\alpha^2}{2}\int_\Omega\nabla\theta\cdot\left((1 - \gamma)I + \gamma\frac{u \otimes u}{|u|^2}\right)\nabla\theta\ud x
\end{equation}
where $I$ is the identity matrix and the scalar $\gamma$ dictates the degree of anisotropy.
When $\gamma = 0$ the smoothing is isotropic, whereas $\gamma = 1$ corresponds to smoothing only along flow.
The anisotropic $H^1$ norm is a good choice when the inferred fields are smoother along-flow than across.
If temperature were the only factor determining the ice fluidity then this functional might be a good candidate for a regularizer.
But the onset of rifts or crevasses marks a sharp discontinuity in the along-flow direction of the fluidity field.
The anisotropic $H^1$ norm would penalize this feature heavily.
Moreover, some features of the temperature field may not align perfectly with the flow direction, such as the thermal effect of a sub-ice shelf melt channel or basal freeze-on zone.

While the TV functional might best encapsulate our prior knowledge for the specific case of inferring ice fluidity, this choice is not universal.
For example, high-resolution measurements of bed topography underneath active ice streams reveal highly streamlined features along-flow \citep{Bingham2017-kg}.
The anisotropic $H^1$ norm would be better suited for estimating fields with these kinds of features.

\section{Conclusion}

The choice of regularization functional in an inverse problem should be guided by a careful consideration of both the physics and the available observational data.
We found that using the TV functional is a substantial improvement over the more commonly used $H^1$ norm for inferring ice fluidity.
Nonetheless, other regularization functionals may be found that improve further on TV in light of better understanding of the physics.
We also found areas suggestive of heavy crevassing that were not evident in previous inversions for the shelf fluidity.
If Larsen C Ice Shelf is in the early stages of an unstable collapse in a manner similar to Larsen B, these regions of basal crevassing may play a prominent role in dictating when and where this breakup occurs.


\appendix
\section{The shallow shelf approximation}

The shallow shelf approximation is a simplification of the full Stokes equations appropriate for floating ice shelves.
The field to be solved for is the ice velocity $u$.
The other main inputs are the ice thickness $h$ and fluidity $A$.
Let $\dot\varepsilon = \text{sym}(\nabla u)$ be the strain rate tensor.
The ice viscosity, assuming Glen's flow law with $n = 3$, is
\begin{equation}
    \mu = \frac{A^{-\frac{1}{n}}}{2}\sqrt{\frac{\text{tr}(\dot\varepsilon^2) + \text{tr}(\dot\varepsilon)^2}{2}}^{\frac{1}{n} - 1}.
\end{equation}
The membrane stress tensor, the analogue of the stress tensor for thin-film flows, is defined as
\begin{equation}
    M = 2\mu\left(\dot\varepsilon + \mathrm{tr}(\dot\varepsilon)\cdot I\right).
\end{equation}
Let $g$ be the acceleration due to gravity, $\rho_I$ the density of ice, $\rho_W$ the density of seawater, and
\begin{equation}
    \varrho = \rho_I(1 - \rho_I / \rho_W)
\end{equation}
the reduced density of ice over water.
Finally, define the \emph{action functional}
\begin{equation}
    J(u) = \int_\Omega\left(\frac{n}{n + 1}hM : \dot\varepsilon - \frac{1}{2}\varrho gh^2\nabla\cdot u\right)\ud x,
\end{equation}
where $M : \dot\varepsilon$ denotes the inner product of rank-two tensors.
Then the shallow shelf equations are the variational derivative of the action functional with respect to $u$.
The action functional is convex, so it has a unique minimizer.
See \citet{shapero2021icepack} for details on the implementation and how it was tested.


\section{Alternating direction method of multipliers}\label{sec:admm}

Using total variation regularization is more challenging than using the squared $H^1$-norm because the TV functional is non-smooth.
This would seem to make it impossible to use gradient-based optimization techniques.
We can circumvent this difficulty by using the \emph{alternating direction method of multipliers} or ADMM.
The idea of ADMM is to introduce an auxiliary vector field $v$ that will take the place of the gradient of the parameters $\nabla\theta$ and ``relax'' the two to be equal by using both a Lagrange multiplier $\mu$ and a penalty.
The penalty will be the squared $L^2$-norm of the mismatch between $v$ and $\nabla\theta$.
The resulting modified Lagrangian is:
\begin{align}
    J_\rho(\theta, u, \lambda; v, \mu) & = E(u - u^o) + R_\alpha(v) + F(u, \theta)\cdot\lambda \\
    & \qquad\quad + \rho\alpha^2\left(\mu\cdot(v - \nabla\theta) + \frac{1}{2}\|v - \nabla\theta\|^2\right)
\end{align}
where $\rho$ is a dimensionless penalty parameter.
The algorithm proceeds in three steps:
\begin{enumerate}
    \item Find a critical point of $J_\rho$ with respect to $\theta$, $u$, and $\lambda$, keeping $\mu$ and $v$ held fixed.
    \item Find a minimizer of $J_\rho$ with respect to $v$ with $\theta$, $u$, and $\lambda$ held fixed.
    \item Perform a gradient ascent step for the multipliers $\mu$.
\end{enumerate}
Step 1 is identical to the $H^1$-regularized problem, but with $\nabla v$ shifted by $v + \mu$.
The solution to the minimization problem in step 2 can be calculated analytically.
Define the \emph{soft threshold} function acting on the vector $z$ is defined as
\begin{equation}
    \text{soft threshold}_{\rho\alpha}(z) = \begin{cases} 0 & \rho\alpha|z| < 1 \\ \left(1 - \frac{1}{\rho\alpha|z|}\right)z & \rho\alpha|z| \ge 1\end{cases}
\end{equation}
Then the solution for $v$ at step 2 is
\begin{equation}
    v \leftarrow \text{soft threshold}_{\rho\alpha}(\nabla\theta + \mu).
\end{equation}
Finally, $\mu$ is updated according to
\begin{equation}
    \mu \leftarrow \mu + \nabla\theta - v.
\end{equation}
It is possible that the method will require many iterations before converging; the chief advantage of ADMM is that it turns a difficult (non-smooth) problem in a sequence of easy (smooth) ones.
See \citet{parikh2014proximal} for a derivation and convergence analysis of the method.

For the Larsen C results shown above, we stopped the ADMM algorithm when the relative change in the log-fluidity with respect to the total variation norm was less than 10${}^{-3}$.
The method had converged to this level after 101 iterations.
There are accelerated proximal methods that give more rapid convergence \citep{parikh2014proximal}, but in the interest of simplicity we used the algorithm presented above.

This same approach can be applied for other non-smooth problems as well; for example, we might instead want to use the $L^1$-norm misfit between the computed velocity $u$ and the observations $u^o$ because we suspect there are large outliers in the measurement errors.


\bibliographystyle{plainnat}
\bibliography{total-variation.bib}

\end{document}
