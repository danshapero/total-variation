
.PHONY: clean

all: total-variation.pdf

total-variation.pdf: total-variation.tex total-variation.bib
	pdflatex total-variation
	bibtex total-variation
	pdflatex total-variation
	pdflatex total-variation

clean:
	rm *.pdf
