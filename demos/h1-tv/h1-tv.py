import numpy as np
from numpy import pi as π
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d

fig = plt.figure()

def add_subplot(fig, *args, **kwargs):
    ax = fig.add_subplot(*args, **kwargs, projection="3d")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    return ax

nx, ny = 128, 128
xs = np.linspace(0.0, 1.0, nx)
ys = np.linspace(0.0, 1.0, ny)
X, Y = np.meshgrid(xs, ys)

r = 0.25
small_tv_small_h1 = np.exp(-((X - 0.5) ** 2 + (Y - 0.5) ** 2) / r ** 2)
ax = add_subplot(fig, 2, 2, 1)
ax.set_title("A", loc="left")
ax.plot_surface(X, Y, small_tv_small_h1, cmap="viridis")

k, l = 3, 3
large_tv_large_h1 = np.sin(π * k * X) ** 2 * np.sin(π * l * Y) ** 2
ax = add_subplot(fig, 2, 2, 2)
ax.set_title("B", loc="left")
ax.plot_surface(X, Y, large_tv_large_h1, cmap="viridis")

small_tv_infinite_h1 = (X > 0.25) & (X < 0.75) & (Y > 0.25) & (Y < 0.75)
ax = add_subplot(fig, 2, 2, 3)
ax.set_title("C", loc="left")
ax.plot_surface(X, Y, small_tv_infinite_h1, cmap="viridis")

Nx, Ny = 5, 5
dx, dy = 0.25 / Nx, 0.25 / Ny
xs = np.linspace(1 / Nx, 1 - 1 / Nx, Nx - 1)
ys = np.linspace(1 / Ny, 1 - 1 / Ny, Ny - 1)
large_tv_infinite_h1 = sum([(X > x - dx) & (X < x + dx) & (Y > y - dy) & (Y < y + dy)
                            for x in xs for y in ys])
ax = add_subplot(fig, 2, 2, 4)
ax.set_title("D", loc="left")
ax.plot_surface(X, Y, large_tv_infinite_h1, cmap="viridis")

fig.tight_layout()
fig.savefig("h1-tv.png", dpi=100, bbox_inches="tight")
