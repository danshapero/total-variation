import os
import subprocess
import numpy as np
import matplotlib.pyplot as plt
import tqdm
import firedrake
from firedrake import assemble, inner, grad, dx, Constant, DumbCheckpoint
import rasterio
import icepack

levels = np.linspace(10e3, 2e3, 9)
input_flag = ""
for index, regularization in tqdm.tqdm(enumerate(levels)):
    output_file = f"larsen-{regularization}.h5"
    command = (
        f"python3 larsen.py {input_flag} --output {output_file} --functional h1"
        f" --regularization {regularization}"
    )
    if not os.path.exists(output_file):
        subprocess.run(command.split(), stdout=subprocess.PIPE)
    input_flag = f"--input larsen-{regularization}.h5"


complexities = np.zeros_like(levels)
errors = np.zeros_like(levels)

mesh = firedrake.Mesh("larsen.msh")
degree = 2
Q = firedrake.FunctionSpace(mesh, "CG", degree)
V = firedrake.VectorFunctionSpace(mesh, "CG", degree)

velocity_filename = icepack.datasets.fetch_measures_antarctica()
with rasterio.open(f"netcdf:{velocity_filename}:VX", "r") as vx, \
     rasterio.open(f"netcdf:{velocity_filename}:VY", "r") as vy, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRX", "r") as errx, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRY", "r") as erry:
    u_obs = icepack.interpolate((vx, vy), V)
    σx = icepack.interpolate(errx, Q)
    σy = icepack.interpolate(erry, Q)

θ = firedrake.Function(Q)
u = firedrake.Function(V)

R = 0.5 * inner(grad(θ), grad(θ)) * dx
E = 0.5 * (((u[0] - u_obs[0]) / σx) ** 2 + ((u[1] - u_obs[1]) / σy) ** 2) * dx

for index, regularization in enumerate(levels):
    with DumbCheckpoint(f"larsen-{regularization}", mode=firedrake.FILE_READ) as chk:
        chk.load(θ, name="log_fluidity")
        chk.load(u, name="velocity")

    complexities[index] = assemble(R)
    errors[index] = assemble(E)

area = assemble(Constant(1) * dx(mesh))

fig, axes = plt.subplots()
axes.scatter(complexities / area, errors / area)
axes.set_xlabel("$\\|\\theta\\|$")
axes.set_ylabel("$\\|u - u^o\\|$")
fig.savefig("l-curve.png", dpi=100)
