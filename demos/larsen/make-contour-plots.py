import os
import argparse
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import cmocean
import rasterio
import firedrake
import icepack, icepack.plot

parser = argparse.ArgumentParser()
parser.add_argument("--regularization", type=float)
args = parser.parse_args()

# Load in the simulation results
mesh = firedrake.Mesh("larsen.msh")
Q = firedrake.FunctionSpace(mesh, "CG", 2)
V = firedrake.VectorFunctionSpace(mesh, "CG", 2)

θ_h1 = firedrake.Function(Q)
θ_tv = firedrake.Function(Q)
u = firedrake.Function(V)

h1_name = f"larsen-h1-{int(args.regularization)}"
tv_name = f"larsen-tv-{int(args.regularization)}"
with firedrake.DumbCheckpoint(h1_name, mode=firedrake.FILE_READ) as chk_h1, \
     firedrake.DumbCheckpoint(tv_name, mode=firedrake.FILE_READ) as chk_tv:
    chk_h1.load(θ_h1, name="log_fluidity")
    chk_tv.load(θ_tv, name="log_fluidity")
    chk_tv.load(u, name="velocity")

# Get a bounding box for the domain
X = mesh.coordinates.dat.data_ro[:]
δ = 50e3
left = X[:, 0].min() - δ
bottom = X[:, 1].min() - δ
right = X[:, 0].max() + δ
top = X[:, 1].max() + δ

# Load in some satellite imagery
image_filename = icepack.datasets.fetch_mosaic_of_antarctica()
with rasterio.open(image_filename, "r") as image_file:
    window = rasterio.windows.from_bounds(
        left=left,
        bottom=bottom,
        right=right,
        top=top,
        width=image_file.width,
        height=image_file.height,
        transform=image_file.transform,
    )
    image = image_file.read(indexes=1, window=window, masked=True)

def subplots(*args, **kwargs):
    fig, axes = icepack.plot.subplots(*args, **kwargs)
    extent = (left, right, bottom, top)

    try:
        axes.imshow(image, cmap="Greys_r", vmin=12e3, vmax=16.38e3, extent=extent)
    except:
        for ax in axes:
            ax.imshow(image, cmap="Greys_r", vmin=12e3, vmax=16.38e3, extent=extent)

    return fig, axes

# Settings for all figures
kwargs = {
    "num_sample_points": 64,
    "cmap": cmocean.cm.ice_r,
    "vmin": math.floor(min(θ_tv.dat.data_ro[:].min(), θ_h1.dat.data_ro[:].min())),
    "vmax": math.ceil(max(θ_tv.dat.data_ro[:].max(), θ_h1.dat.data_ro[:].max())),
}

# Big comparison figure
filename = f"parameters-{int(args.regularization)}.png"
if not os.path.exists(filename):
    fig, axes = subplots(nrows=1, ncols=2, sharex=True, sharey=True, figsize=(6, 3))
    icepack.plot.tripcolor(θ_h1, axes=axes[0], **kwargs)
    colors = icepack.plot.tripcolor(θ_tv, axes=axes[1], **kwargs)
    fig.suptitle(
        "Log-fluidity of Larsen C Ice Shelf with\n different regularization functionals"
    )

    axes[0].set_ylabel("meters")
    axes[1].get_xaxis().set_ticklabels([])

    axes[0].set_title("H${}^1$")
    axes[1].set_title("TV")

    fig.colorbar(colors, ax=axes, shrink=0.5)
    fig.savefig(filename, dpi=150, bbox_inches="tight")

# Detail figure around Gipps Ice Rise rift
gipps_bbox = {"x": (-2.125e6, -2.04e6), "y": (1.11e6, 1.175e6)}
filename = f"gipps-{int(args.regularization)}.png"
if not os.path.exists(filename):
    fig, axes = subplots(nrows=1, ncols=2, sharex=True, sharey=True, figsize=(6, 2.5))
    for ax in axes:
        ax.set_xlim(gipps_bbox["x"])
        ax.set_ylim(gipps_bbox["y"])
        ax.get_xaxis().set_ticklabels([])
        ax.get_yaxis().set_ticklabels([])
    icepack.plot.tripcolor(θ_h1, axes=axes[0], **kwargs)
    icepack.plot.tripcolor(θ_tv, axes=axes[1], **kwargs)
    fig.savefig(filename, dpi=150, bbox_inches="tight")

# Detail figure around inflow boundary
inflow_bbox = {"x": (-2.338e6, -2.17e6), "y": (1.065e6, 1.195e6)}
filename = f"inflow-{int(args.regularization)}.png"
if not os.path.exists(filename):
    fig, axes = subplots(ncols=1, nrows=2, sharex=True, sharey=True, figsize=(2.5, 4.5))
    for ax in axes:
        ax.set_xlim(inflow_bbox["x"])
        ax.set_ylim(inflow_bbox["y"])
        ax.get_xaxis().set_ticklabels([])
        ax.get_yaxis().set_ticklabels([])
    icepack.plot.tripcolor(θ_h1, axes=axes[0], **kwargs)
    icepack.plot.tripcolor(θ_tv, axes=axes[1], **kwargs)
    fig.savefig(filename, dpi=150, bbox_inches="tight")

# Figure to show detail locations
filename = "larsen.png"
if not os.path.exists(filename):
    fig, axes = subplots()
    firedrake.streamplot(u, seed=1729, resolution=4e3, cmap=cmocean.cm.ice, axes=axes)

    for bbox in [inflow_bbox]:
        width = bbox["x"][1] - bbox["x"][0]
        height = bbox["y"][1] - bbox["y"][0]
        origin = (bbox["x"][0], bbox["y"][0])
        rectangle = Rectangle(origin, width, height, fill=False, edgecolor="r")
        axes.add_patch(rectangle)

    text = {
        "C": (-2.36e6, 1.12e6),
        "M": (-2.3e6, 1.05e6),
        "W": (-2.27e6, 1.01e6),
        "S": (-2.23e6, 0.989e6),
        "T": (-2.193e6, 0.976e6),
        "HK": (-2.115e6, 1.1e6),
        "G": (-2.08e6, 1.152e6),
    }

    for label, coord in text.items():
        axes.text(*coord, label, color="tab:blue")

    fig.savefig("larsen.png", dpi=150, bbox_inches="tight")
