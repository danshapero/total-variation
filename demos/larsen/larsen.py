import os
import glob
import argparse
import subprocess
import numpy as np
import rasterio
import geojson
import firedrake
from firedrake import sqrt, inner, grad, dx, assemble
import icepack
from icepack.constants import glen_flow_law as n


parser = argparse.ArgumentParser()
parser.add_argument("--functional", choices=["h1", "tv"])
parser.add_argument("--regularization", type=float)
parser.add_argument("--input")
parser.add_argument("--output")
args = parser.parse_args()

# Read in the geometry and generate a mesh.
outline_filename = icepack.datasets.fetch_outline("larsen")
with open(outline_filename, "r") as outline_file:
    outline = geojson.load(outline_file)

geometry = icepack.meshing.collection_to_geo(outline)
with open("larsen.geo", "w") as geo_file:
    geo_file.write(geometry.get_code())

if not os.path.exists("larsen.msh"):
    subprocess.run("gmsh -2 -format msh2 -v -2 -o larsen.msh larsen.geo".split())

# Read in the mesh and create some function spaces
mesh = firedrake.Mesh("larsen.msh")
degree = 2
Q = firedrake.FunctionSpace(mesh, "CG", degree)
V = firedrake.VectorFunctionSpace(mesh, "CG", degree)

# Read in the thickness map and smooth it a bit
thickness_filename = icepack.datasets.fetch_bedmachine_antarctica()
with rasterio.open(f"netcdf:{thickness_filename}:thickness", "r") as thickness:
    h_obs = icepack.interpolate(thickness, Q)

h = firedrake.Function(Q)
α_h = firedrake.Constant(2e3)
J = 0.5 * ((h - h_obs) ** 2 + α_h ** 2 * inner(grad(h), grad(h))) * dx
params = {
    "ksp_type": "preonly",
    "pc_type": "lu",
    "pc_factor_mat_solver_type": "mumps",
}
firedrake.solve(firedrake.derivative(J, h) == 0, h, solver_parameters=params)

# Read in the velocity map and the estimates for the observational errors
velocity_filename = icepack.datasets.fetch_measures_antarctica()
with rasterio.open(f"netcdf:{velocity_filename}:VX", "r") as vx, \
     rasterio.open(f"netcdf:{velocity_filename}:VY", "r") as vy, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRX", "r") as errx, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRY", "r") as erry:
    u_obs = icepack.interpolate((vx, vy), V)
    σx = icepack.interpolate(errx, Q)
    σy = icepack.interpolate(erry, Q)


def viscosity(**kwargs):
    u = kwargs["velocity"]
    h = kwargs["thickness"]
    θ = kwargs["log_fluidity"]

    T = firedrake.Constant(260)
    A0 = icepack.rate_factor(T)
    A = A0 * firedrake.exp(θ)
    return icepack.models.viscosity.viscosity_depth_averaged(
        velocity=u, thickness=h, fluidity=A
    )


θ = firedrake.Function(Q)
u_init = firedrake.Function(V)

if args.input:
    input_name = os.path.splitext(args.input)[0]
    with firedrake.DumbCheckpoint(input_name, mode=firedrake.FILE_READ) as chk:
        chk.load(θ, name="log_fluidity")
        chk.load(u_init, name="velocity")
else:
    u_init.assign(u_obs)


flow_model = icepack.models.IceShelf(viscosity=viscosity)
opts = {"dirichlet_ids": [2, 4, 5, 6, 7, 8, 9], "tolerance": 1e-12}
flow_solver = icepack.solvers.FlowSolver(flow_model, **opts)
u = flow_solver.diagnostic_solve(velocity=u_init, thickness=h, log_fluidity=θ)


def objective(u):
    δu = u - u_obs
    return 0.5 * ((δu[0] / σx) ** 2 + (δu[1] / σy) ** 2) * dx


α = firedrake.Constant(args.regularization)
ρ = firedrake.Constant(1.0)

# TODO: Check that this degree is correct!
Δ = firedrake.VectorFunctionSpace(mesh, "CG", degree)
v = firedrake.Function(Δ)
μ = firedrake.Function(Δ)


def regularization(θ):
    return 0.5 * ρ * α ** 2 * inner(grad(θ) - v + μ, grad(θ) - v + μ) * dx


problem = icepack.inverse.InverseProblem(
    model=flow_model,
    objective=objective,
    regularization=regularization,
    state_name="velocity",
    state=u,
    parameter_name="log_fluidity",
    parameter=θ,
    solver_kwargs=opts,
    diagnostic_solve_kwargs={"thickness": h},
)

area = assemble(firedrake.Constant(1) * dx(mesh))


def callback(solver):
    dJ = solver.gradient
    φ = solver.search_direction
    dJ_dφ = firedrake.action(dJ, φ)
    Δ = firedrake.assemble(dJ_dφ, form_compiler_parameters=params)

    E = assemble(solver.objective)
    R = assemble(solver.regularization)
    print(f"    | {E / area:g}, {R / area:g}, {Δ / area:g}")


solver = icepack.inverse.GaussNewtonSolver(problem, callback, search_max_iterations=500)


def converged(θs, tolerance):
    if len(θs) < 2:
        return False
    if args.functional == "h1":
        return True

    δθ = θs[-1] - θs[-2]
    absolute_change = assemble(sqrt(inner(grad(δθ), grad(δθ))) * dx)
    norm = assemble(sqrt(inner(grad(θs[-1]), grad(θs[-1]))) * dx)
    relative_change = absolute_change / norm
    return relative_change < tolerance


θs = [θ.copy(deepcopy=True)]
iteration_count = 0
tolerance = 1e-3
while not converged(θs, tolerance):
    # Solve the inverse problem with H1 regularization
    iterations = solver.solve(rtol=1e-4, etol=1e-8, atol=0.0, max_iterations=30)

    θ = solver.parameter
    θs.append(θ.copy(deepcopy=True))

    # v <- soft threshold(grad(θ) + μ, ρ * α)
    z = grad(θ) + μ
    expr = firedrake.conditional(
        (ρ * α) ** 2 * inner(z, z) < 1,
        firedrake.Constant((0.0, 0.0)),
        (1 - 1 / (ρ * α * sqrt(inner(z, z)))) * z,
    )
    v.project(expr)

    # gradient ascent step for μ
    μ.project(μ + grad(θ) - v)

    δθ = θs[-1] - θs[-2]
    absolute_change = assemble(sqrt(inner(grad(δθ), grad(δθ))) * dx)
    norm = assemble(sqrt(inner(grad(θs[-1]), grad(θs[-1]))) * dx)
    relative_change = absolute_change / norm
    iteration_count += 1
    print(f"{iteration_count:3d} | {relative_change}")


output_name = os.path.splitext(args.output)[0]
with firedrake.DumbCheckpoint(output_name, mode=firedrake.FILE_CREATE) as chk:
    chk.store(solver.parameter, name="log_fluidity")
    chk.store(solver.state, name="velocity")
