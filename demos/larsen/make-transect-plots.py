import os
import argparse
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import cmocean
import rasterio
import firedrake
from firedrake import inner, grad, dx
import icepack, icepack.plot

mesh = firedrake.Mesh("larsen.msh")
Q = firedrake.FunctionSpace(mesh, "CG", 2)
V = firedrake.VectorFunctionSpace(mesh, "CG", 2)

θ = firedrake.Function(Q)
u = firedrake.Function(V)

regularization = 7500
tv_name = f"larsen-tv-{int(regularization)}"
with firedrake.DumbCheckpoint(tv_name, mode=firedrake.FILE_READ) as chk_tv:
    chk_tv.load(θ, name="log_fluidity")
    chk_tv.load(u, name="velocity")

# Get a bounding box for the domain
X = mesh.coordinates.dat.data_ro[:]
δ = 50e3
left = X[:, 0].min() - δ
bottom = X[:, 1].min() - δ
right = X[:, 0].max() + δ
top = X[:, 1].max() + δ

# Load in some satellite imagery
image_filename = icepack.datasets.fetch_mosaic_of_antarctica()
with rasterio.open(image_filename, "r") as image_file:
    window = rasterio.windows.from_bounds(
        left=left,
        bottom=bottom,
        right=right,
        top=top,
        width=image_file.width,
        height=image_file.height,
        transform=image_file.transform,
    )
    image = image_file.read(indexes=1, window=window, masked=True)

def subplots(*args, **kwargs):
    fig, axes = icepack.plot.subplots(*args, **kwargs)
    extent = (left, right, bottom, top)

    try:
        axes.imshow(image, cmap="Greys_r", vmin=12e3, vmax=16.38e3, extent=extent)
    except:
        for ax in axes:
            ax.imshow(image, cmap="Greys_r", vmin=12e3, vmax=16.38e3, extent=extent)

    return fig, axes

# Settings for all figures
kwargs = {
    "num_sample_points": 64,
    "cmap": cmocean.cm.ice_r,
    "vmin": math.floor(min(θ.dat.data_ro[:].min(), θ.dat.data_ro[:].min())),
    "vmax": math.ceil(max(θ.dat.data_ro[:].max(), θ.dat.data_ro[:].max())),
}

thickness_filename = icepack.datasets.fetch_bedmachine_antarctica()
with rasterio.open(f"netcdf:{thickness_filename}:thickness", "r") as thickness:
    h_obs = icepack.interpolate(thickness, Q)

h = firedrake.Function(Q)
α_h = firedrake.Constant(2e3)
J = 0.5 * ((h - h_obs) ** 2 + α_h ** 2 * inner(grad(h), grad(h))) * dx
params = {
    "ksp_type": "preonly",
    "pc_type": "lu",
    "pc_factor_mat_solver_type": "mumps",
}
firedrake.solve(firedrake.derivative(J, h) == 0, h, solver_parameters=params)

velocity_filename = icepack.datasets.fetch_measures_antarctica()
with rasterio.open(f"netcdf:{velocity_filename}:VX", "r") as vx, \
     rasterio.open(f"netcdf:{velocity_filename}:VY", "r") as vy, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRX", "r") as errx, \
     rasterio.open(f"netcdf:{velocity_filename}:ERRY", "r") as erry:
    u_obs = icepack.interpolate((vx, vy), V)
    σx = icepack.interpolate(errx, Q)
    σy = icepack.interpolate(erry, Q)

x_start = (-2.237e6, 1.069e6)
x_end = (-2.189e6, 1.129e6)
num_points = 200
x = np.linspace(x_start[0], x_end[0], num_points)
y = np.linspace(x_start[1], x_end[1], num_points)

fig, axes = subplots()
icepack.plot.tripcolor(θ, axes=axes, **kwargs)
axes.plot(x, y)
fig.savefig("transect-location.png", dpi=150, bbox_inches="tight")

θs = np.array([θ.at(X) for X in zip(x, y)])
us = np.array([u.at(X) for X in zip(x, y)])
hs = np.array([h.at(X) for X in zip(x, y)])
h_os = np.array([h_obs.at(X) for X in zip(x, y)])
u_os = np.array([u_obs.at(X) for X in zip(x, y)])
σxs = np.array([σx.at(X) for X in zip(x, y)])
σys = np.array([σy.at(X) for X in zip(x, y)])

fig, axes1 = plt.subplots()
color1 = "tab:blue"
axes1.plot(θs, color=color1)
axes1.set_ylabel("log-fluidity (dimensionless)", color=color1)
axes2 = axes1.twinx()
color2 = "tab:orange"
axes2.plot(hs, color=color2, label="smoothed")
axes2.plot(h_os, '--', color=color2, label="observed")
axes2.legend()
axes2.set_ylabel("ice thickness (meters)", color=color2)
fig.savefig("transect-thickness-fluidity.png", dpi=150, bbox_inches="tight")

fig, axes = plt.subplots()
axes.plot(np.sqrt(np.sum(us**2, axis=1)), color="tab:blue", label="computed")
axes.plot(np.sqrt(np.sum(u_os**2, axis=1)), color="tab:orange", label="observed")
axes.set_ylabel("ice speed (meters / year)")
axes.legend()
fig.savefig("transect-velocity.png", dpi=150, bbox_inches="tight")

fig, axes = plt.subplots()
χ = np.sqrt(((us[:, 0] - u_os[:, 0]) / σxs) ** 2 + ((us[:, 1] - u_os[:, 1]) / σys) **2)
axes.plot(χ, color="tab:blue")
axes.set_ylabel("ice velocity difference (meters / year)")
fig.savefig("transect-velocity-diff.png", dpi=150, bbox_inches="tight")
